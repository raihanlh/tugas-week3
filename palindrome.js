const palindrome = (kata) => {
    let n_kata = kata.length;

    for (let i = 0; i <= Math.trunc(n_kata/2); i++) {
        if (kata[i] !== kata[n_kata-i-1])
            return false
    }
    return true
}

// TEST CASES
console.log(palindrome('katak')); // true
console.log(palindrome('blanket')); // false
console.log(palindrome('civic')); // true
console.log(palindrome('kasur rusak')); // true
console.log(palindrome('mister')); // false