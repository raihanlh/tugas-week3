// Objectives

//     Mampu menggunakan built in function pada Array seperti .push, .shift, .unshift, dll
//     Mampu membuat function dan mengerti penggunaan parameter dari sebuah function
//     Mampu menggunakan template literals

// RESTRICTION Hanya boleh menggunakan built-in function untuk menambahkan atau mengurangi data dalam array, seperti .shift(), unShift(), push(), dan pop()
// Directions

// Ganjil Genap
// Diberikan sebuah function ganjilGenap yang menerima satu parameter plat bertipe string. Parameter plat berisi informasi kumpulan plat dimana nomor antar plat dipisahkan oleh titik koma(;).
// Function ini akan mengembalikan keterangan jumlah plat genap dan jumlah plat ganjil.
function ganjilGenap(plat) {
  // your code here
  if (plat === undefined)
    return "invalid data"
  else if (plat === "")
    return "plat tidak ditemukan"
  else {
    let arr = plat.split(";");
    let n_ganjil = 0;
    let n_genap = 0;

    // Pop array and check until array is empty
    while (arr.length > 0) {
      let num = arr.pop()
      parseInt(num) % 2 === 0 ? n_genap++ : n_ganjil++;
    }

    let genapSentence;
    let ganjilSentence;

    // Form answer
    genapSentence = n_genap > 0 ? `plat genap sebanyak ${n_genap}` : `plat genap tidak ditemukan`;
    ganjilSentence = n_ganjil > 0 ? `plat ganjil sebanyak ${n_ganjil}` : `plat ganjil tidak ditemukan`;

    return (n_genap === 0 ? ganjilSentence + " dan " + genapSentence : genapSentence + " dan " + ganjilSentence);
  }
}
  
console.log(ganjilGenap('2341;3429;864;1309;1276')) //plat genap sebanyak 2 dan plat ganjil sebanyak 3
console.log(ganjilGenap('2347;3429;1305')) //plat ganjil sebanyak 3 dan plat genap tidak ditemukan
console.log(ganjilGenap('864;1308;1276;1432')) //plat genap sebanyak 4 dan plat ganjil tidak ditemukan
console.log(ganjilGenap('')) //plat tidak ditemukan
console.log(ganjilGenap()) //invalid data