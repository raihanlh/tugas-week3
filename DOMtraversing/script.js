let eldestParent = document.getElementById("eldest-parent");

let firstChild = eldestParent.children[0];
firstChild.innerHTML = "<div>Diakses Melalui Eldest Parent</div>";

let aChild = document.getElementById("a-child");

let prevSibling = aChild.previousElementSibling;
prevSibling.innerHTML = "<div>Diakses Melalui a Child</div>";
let nextSibling = aChild.nextElementSibling;
nextSibling.innerHTML = "<div>Diakses Melalui a Child</div>";

let oldGen = aChild.parentNode.parentNode.nextElementSibling;
oldGen.innerHTML = "<div>Diakses Melalui a Child</div>";