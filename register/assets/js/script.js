$(() => {
    $("#main-form").validate({
        rules: {
            username: {
                required: true,
                minlength: 4
            },
            password: {
                required: true,
                minlength: 6
            },
            confirm_password: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
            confirm_email: {
                required: true,
                email: true,
                equalTo: "#email"
            }
        },
        messages: {
            username: {
                required: "Please enter a username",
                minlength: "Your username must be 4 characters or longer"
            },
            password: {
                required: "Please enter a password",
                minlength: "Your password must be 6 characters or longer"
            },
            confirm_password: {
                required: "Please confirm your password",
                minlength: "Your password must be 6 characters or longer",
                equalTo: "Password doesn't match"
            },
            email: "Please enter a valid email address",
            confirm_email: {
                required: "Please enter a valid email address",
                equalTo: "Email doesn't match"
            }
        },
        submitHandler: (form) => {
            alert("Registration success!")
            form.submit();
        }
    })
})